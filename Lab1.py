import math

# Блок ввода данных
a = float(input('Введите  переменную а: '))
x = float(input('Введите переменную х: '))

# Блок условия
try:
    # Блок рассчёта значений функций
    G = (6 * (4 * a ** 2 - 12 * a * x + 5 * x ** 2)) / (9 * a ** 2 + 30 * a * x + 16 * x ** 2)
    F = 5 ** (a ** 2 - 5 * a * x + 4 * x ** 2)
    Y = math.log(-a ** 2 - 7 * a * x + 8 * x ** 2 + 1) / math.log(2)

    print('G = {:.5f}'. format(G))
    print('F = {:.5f}'. format(F))
    print('Y = {:.5f}'. format(Y))

except ZeroDivisionError:
    print ('Одна из функций не может решится из-за деления на ноль')
except ValueError:
    print('Одна из функций пытается вычислить функцию с минусовым значением')
